# do magic stuff

install_defaults.package:
  pkg.installed:
    - pkgs:
      - htop
      - bash

remove_default.packages:
  pkg.removed:
    - pkgs:
      - telnet
